package de.richtercloud.primefaces.type.error;

import java.io.Serializable;
import java.util.HashMap;
import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class Entity0View implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final String ENTRY_0_VALUE = "value0";
    private static final String ENTITY_1_A_NAME = "entity1a";
    private static final String ENTITY_1_B_NAME = "entity1b";
    private static final String ENTITY_1_C_NAME = "entity1c";
    private static final String ENTRY_0_NAME = "entry0";
    private static final String ENTRY_1_0_NAME = "entry1-0";
    private static final String CONTEXT_0_NAME = "context0";
    private static final String ENTRY_1_NAME = "enty1";
    private static final String ENTRY_1_0_VALUE = "value1-0";
    private Entity0 entity1;
    private boolean editMode = false;
    public static Entity1 entity1a;
    public static Entity1 entity1b;
    static {
        init0();
    }

    public static void init0() {
        entity1a = new Entity1(ENTITY_1_A_NAME);
        entity1b = new Entity1(ENTITY_1_B_NAME);
    }

    @PostConstruct
    private void init() {
        Entity1 entity1c = new Entity1(ENTITY_1_C_NAME);
        this.entity1 = new Entity0(CONTEXT_0_NAME,
                entity1c,
                new HashMap<String, ValueWrapper>() {{
                        put(ENTRY_0_NAME, new StringValue(ENTRY_0_VALUE));
                        put(ENTRY_1_NAME, new ReferenceValue(entity1a,
                                new HashMap<String, ValueWrapper>() {{
                                        put(ENTRY_1_0_NAME, new StringValue(ENTRY_1_0_VALUE));
                                }}));
                }},
                0);
    }

    public Entity0 getEntity1() {
        return entity1;
    }

    public void setEntity1(Entity0 entity1) {
        this.entity1 = entity1;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public void enableEditMode(AjaxBehaviorEvent event) {
        this.editMode = true;
    }
}
