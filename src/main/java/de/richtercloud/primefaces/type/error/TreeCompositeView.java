package de.richtercloud.primefaces.type.error;

import java.io.Serializable;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@Named
@ViewScoped
public class TreeCompositeView implements Serializable {
    private static final long serialVersionUID = 1L;
    private final Map<Entry<Entity0, String>, TreeNode> rootMap = new HashMap<>();

    public TreeNode createNodes(Entity0 offer,
            String offerContextName,
            Map<String, ValueWrapper> valueMap) {
        Entry<Entity0, String> rootMapKey = new SimpleEntry<>(offer, offerContextName);
        TreeNode root = rootMap.get(rootMapKey);
        if(root == null) {
            //Lazy initialization of the root node is an absolute necessity, see
            //https://stackoverflow.com/questions/50234548/why-do-value-updates-fail-for-components-below-roots-in-a-ptreetable?noredirect=1#comment87498081_50234548
            //for details
            root = new DefaultTreeNode(new TreeNodeData("",
                            null
                    ), //data (values are never used
                    null //parent
            );
            createNodesRecurse(valueMap,
                    root);
            rootMap.put(rootMapKey, root);
        }
        return root;
    }

    private void createNodesRecurse(Map<String, ValueWrapper> valueMap,
            TreeNode root) {
        for(Entry<String, ValueWrapper> offerTypeContext : valueMap.entrySet()) {
            if(offerTypeContext.getValue() instanceof StringValue) {
                new DefaultTreeNode(new TreeNodeData(offerTypeContext.getKey(),
                                valueMap.get(offerTypeContext.getValue())),
                        root //parent
                );
            }else if(offerTypeContext.getValue() instanceof ReferenceValue) {
                ReferenceValue valueWrapperCast = (ReferenceValue) offerTypeContext.getValue();
                TreeNode root0 = new DefaultTreeNode(new TreeNodeData(offerTypeContext.getKey(),
                                valueWrapperCast),
                        root //parent
                );
                List<Entity1> referenceItems = new LinkedList<>(Arrays.asList(Entity0View.entity1a,
                        Entity0View.entity1b));
                if(valueWrapperCast.getSelectedEntity1() == null && !referenceItems.isEmpty()) {
                    valueWrapperCast.setSelectedEntity1(referenceItems.get(0));
                }
                Map<String, ValueWrapper> valueMap0 = valueWrapperCast.getValueMap();
                createNodesRecurse(valueMap0,
                        root0);
            }else {
                throw new IllegalArgumentException("offer properties of type re not supported");
            }
        }
    }

    public void onOfferTypeChange(ReferenceValue referenceValue,
            Entity0 offer,
            String offerContextName) {
        this.rootMap.remove(new SimpleEntry<>(offer, offerContextName));
    }
}
