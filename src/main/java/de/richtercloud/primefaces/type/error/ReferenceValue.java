package de.richtercloud.primefaces.type.error;

import java.util.HashMap;
import java.util.Map;

public class ReferenceValue extends ValueWrapper {
    private Entity1 selectedEntity1;
    private Map<String, ValueWrapper> valueMap;

    public ReferenceValue() {
        this(null,
                new HashMap<>());
    }

    public ReferenceValue(Entity1 selectedEntity1,
            Map<String, ValueWrapper> valueMap) {
        this.selectedEntity1 = selectedEntity1;
        this.valueMap = valueMap;
    }

    public Entity1 getSelectedEntity1() {
        return selectedEntity1;
    }

    public void setSelectedEntity1(Entity1 selectedEntity1) {
        this.selectedEntity1 = selectedEntity1;
    }

    public Map<String, ValueWrapper> getValueMap() {
        return valueMap;
    }

    public void setValueMap(Map<String, ValueWrapper> valueMap) {
        this.valueMap = valueMap;
    }
}
