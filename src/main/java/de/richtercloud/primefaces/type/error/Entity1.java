package de.richtercloud.primefaces.type.error;

public class Entity1 {
    private String name;

    public Entity1() {
    }

    public Entity1(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
