package de.richtercloud.primefaces.type.error;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Entity0 {
    private String context;
    private Entity1 offerType;
    private Map<String, ValueWrapper> valueMap = new HashMap<>();

    public Entity0() {
    }

    public Entity0(String context,
            Entity1 offerType,
            int index) {
        this(context,
                offerType,
                new HashMap<>(),
                index);
    }

    public Entity0(String context,
            Entity1 offerType,
            Map<String, ValueWrapper> valueMap,
            int index) {
        if(context == null) {
            throw new IllegalArgumentException("context mustn't be null");
        }
        if(offerType == null && !context.isEmpty()) {
            throw new IllegalArgumentException("offerType == null is only allowed for empty context");
        }
        if(index < 0) {
            throw new IllegalArgumentException("index mustn't be negative");
        }
        this.context = context;
        this.offerType = offerType;
        this.valueMap = valueMap;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Entity1 getOfferType() {
        return offerType;
    }

    public void setOfferType(Entity1 offerType) {
        this.offerType = offerType;
    }

    public Map<String, ValueWrapper> getValueMap() {
        return valueMap;
    }

    public void setValueMap(Map<String, ValueWrapper> valueMap) {
        this.valueMap = valueMap;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.context);
        hash = 83 * hash + Objects.hashCode(this.offerType);
        hash = 83 * hash + Objects.hashCode(this.valueMap);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entity0 other = (Entity0) obj;
        if (!Objects.equals(this.context, other.context)) {
            return false;
        }
        if (!Objects.equals(this.offerType, other.offerType)) {
            return false;
        }
        if (!Objects.equals(this.valueMap, other.valueMap)) {
            return false;
        }
        return true;
    }
}
