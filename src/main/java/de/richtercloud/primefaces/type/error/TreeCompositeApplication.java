package de.richtercloud.primefaces.type.error;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

@Named
@ApplicationScoped
public class TreeCompositeApplication implements Serializable {
    private static final long serialVersionUID = 1L;
    private Converter offerTypeConverter = new Converter() {
        private final Map<String, Entity1> map = new HashMap<>();

        @Override
        public Entity1 getAsObject(FacesContext context,
                UIComponent component,
                String value) {
            if(context == null) {
                throw new NullPointerException("context mustn't be null");
                    //according to interface specification
            }
            if(component == null) {
                throw new NullPointerException("component mustn't be null");
                    //according to interface specification
            }
            if(context == null) {
                throw new NullPointerException("context mustn't be null");
            }
            if(value == null) {
                return null;
            }
            Entity1 retValue0 = map.get(value);
            return retValue0;
        }

        @Override
        public String getAsString(FacesContext context,
                UIComponent component,
                Object value) {
            if(context == null) {
                throw new NullPointerException("context mustn't be null");
                    //according to interface specification
            }
            if(component == null) {
                throw new NullPointerException("component mustn't be null");
                    //according to interface specification
            }
            if(value == null) {
                return "";
                    //according to interface specification
            }
            Entity1 valueCast = (Entity1)value;
            map.put(valueCast.getName(), valueCast);
            return valueCast.getName();
        }
    };

    public Converter getOfferTypeConverter() {
        return offerTypeConverter;
    }

    public void setOfferTypeConverter(Converter offerTypeConverter) {
        this.offerTypeConverter = offerTypeConverter;
    }

    public boolean isDisplayStringInput(ValueWrapper offerProperty) {
        boolean retValue = offerProperty instanceof StringValue;
        return retValue;
    }

    public boolean isDisplayReferenceInput(ValueWrapper offerProperty) {
        boolean retValue = offerProperty instanceof ReferenceValue;
        return retValue;
    }

    public List<Entity1> createReferenceItems() {
        List<Entity1> retValue0 = new LinkedList<>(Arrays.asList(Entity0View.entity1a,
                Entity0View.entity1b));
        return retValue0;
    }

    public String retrieveValueText(ValueWrapper valueWrapper) {
        if(valueWrapper == null) {
            return ""; //TODO: never happened before adding rowKey to TreeNodes,
                    //so this is like JSF nonsense
        }
        if(valueWrapper instanceof StringValue) {
            return String.valueOf(((StringValue)valueWrapper).getTheValue());
        }else if(valueWrapper instanceof ReferenceValue) {
            return ((ReferenceValue)valueWrapper).getSelectedEntity1().getName();
        }else {
            throw new IllegalArgumentException(String.format("value wrappers of type %s are not supported",
                    valueWrapper.getClass()));
        }
    }
}
