package de.richtercloud.primefaces.type.error;

import java.io.Serializable;
import java.util.Objects;

public class TreeNodeData implements Serializable {
    private String context;
    private ValueWrapper valueWrapper;

    public TreeNodeData() {
    }

    public TreeNodeData(String context,
            ValueWrapper valueWrapper) {
        this.context = context;
        this.valueWrapper = valueWrapper;
    }

    public String getContext() {
        return context;
    }

    public ValueWrapper getValueWrapper() {
        return valueWrapper;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public void setValueWrapper(ValueWrapper valueWrapper) {
        this.valueWrapper = valueWrapper;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.context);
        hash = 17 * hash + Objects.hashCode(this.valueWrapper);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreeNodeData other = (TreeNodeData) obj;
        if (!Objects.equals(this.context, other.context)) {
            return false;
        }
        if (!Objects.equals(this.valueWrapper, other.valueWrapper)) {
            return false;
        }
        return true;
    }
}
