package de.richtercloud.primefaces.type.error;

public class StringValue extends ValueWrapper {
    private String theValue;

    public StringValue() {
    }

    public StringValue(String theValue) {
        this.theValue = theValue;
    }

    public String getTheValue() {
        return theValue;
    }

    public void setTheValue(String theValue) {
        this.theValue = theValue;
    }
}
